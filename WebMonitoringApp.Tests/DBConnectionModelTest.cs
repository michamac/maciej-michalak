// <copyright file="DBConnectionModelTest.cs">Copyright ©  2018</copyright>
using System;
using System.Data.Common;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebMonitoringApp.Models;

namespace WebMonitoringApp.Models.Tests
{
    /// <summary>This class contains parameterized unit tests for DBConnectionModel</summary>
    [PexClass(typeof(DBConnectionModel))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class DBConnectionModelTest
    {
        /// <summary>Test stub for CreateDbConnection(String, String)</summary>
        [PexMethod]
        public DbConnection CreateDbConnectionTest(
            [PexAssumeUnderTest]DBConnectionModel target,
            string providerName,
            string connectionString
        )
        {
            DbConnection result = target.CreateDbConnection(providerName, connectionString);
            return result;
            // TODO: add assertions to method DBConnectionModelTest.CreateDbConnectionTest(DBConnectionModel, String, String)
        }
    }
}
