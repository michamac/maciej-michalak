﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMonitoringApp.Models
{

    public class ChartModel
    {
        public class Dataset
        {
            protected string[] bgcolors = {
                "rgb(255, 0, 0, 0.2)",
                "rgb(255, 128, 0, 0.2)",
                "rgb(255, 255, 0, 0.2)",
                "rgb(128, 255, 0, 0.2)",
                "rgb(0, 255, 191, 0.2)",
                "rgb(0, 191, 255, 0.2)",
                "rgb(0, 64, 255, 0.2)",
                "rgb(128, 0, 255, 0.2)",
                "rgb(255, 0, 255, 0.2)",
                "rgb(255, 0, 128, 0.2)"
            };

            private string[] bdcolors = {
               "rgb(255, 0, 0)",
               "rgb(255, 128, 0)",
               "rgb(rgb(255, 255, 0)",
               "rgb(128, 255, 0)",
               "rgb(0, 255, 191)",
               "rgb(0, 191, 255)",
               "rgb(0, 64, 255)",
               "rgb(128, 0, 255)",
               "rgb(255, 0, 255)",
               "rgb(255, 0, 128)"
            };

            protected int bgcolornr = 1;
            private int bdcolornr = 1;


            public string label { get; set; }

            public object[] data { get; set; }
            public string backgroundColor
            {
                set
                {
                    bgcolornr = Convert.ToInt32(value);
                }
                get
                {
                    return bgcolors[bgcolornr];
                }
            }
            public string borderColor
            {
                set
                {
                    bdcolornr = Convert.ToInt32(value);
                }
                get
                {
                    return bdcolors[bdcolornr];
                }
            }
            public int borderWidth { get; set; }

        }
        public List<Dataset> Datasets;
        public string ServerName;
        public string CounterName;
        public object DatasetLabels
        {
            get
            {
                return Datasets.Where((x, index) => x.label.ToString() != null);
                      
            }
        }
        public string[] Labels;

        public ChartModel()
        {
            Datasets = new List<Dataset>();
        }
    }
   

}
