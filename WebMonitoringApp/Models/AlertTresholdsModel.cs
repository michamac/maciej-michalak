﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMonitoringApp.Models
{
    public class AlertTresholdModels
    {
        public string ServerName;
        public string CounterName;
        public string MaxValue;
        public string MinValue;
        public bool Active;
    }
}