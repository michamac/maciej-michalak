﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Common;
using System.Configuration;
using System.Data;

namespace WebMonitoringApp.Models
{
    public class DBConnectionModel
    {
        public DbConnection Connection { get; set; }
        public string ConnectionString { get; }
        public string Provider { get; set; }
        public DbDataReader Reader { get; set; }
        public string DBName;
        //public string Query { get; set; }

        public DBConnectionModel(string provider)
        {
            Provider = provider;
            ConnectionString = GetConnectionStringByProvider(provider);
            //Connection = CreateDbConnection(Provider, ConnectionString);
            Reader = null;
            //Connection.Open();
        }

        private string GetConnectionStringByProvider(string providerName)
        {
            // Return null on failure.
            string returnValue = null;

            // Get the collection of connection strings.
            ConnectionStringSettingsCollection settings =
                ConfigurationManager.ConnectionStrings;

            // Walk through the collection and return the first 
            // connection string matching the providerName.
            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    if (cs.ProviderName == providerName)
                        returnValue = cs.ConnectionString;
                    break;
                }
            }
            return returnValue;
        }


        // Given a provider name and connection string, 
        // create the DbProviderFactory and DbConnection.
        // Returns a DbConnection on success; null on failure.
        public DbConnection CreateDbConnection(
            string providerName, string connectionString)
        {
            // Assume failure.
            DbConnection connection = null;

            // Create the DbProviderFactory and DbConnection.
            if (connectionString != null)
            {
                try
                {
                    DbProviderFactory factory =
                        DbProviderFactories.GetFactory(providerName);

                    connection = factory.CreateConnection();
                    connection.ConnectionString = connectionString;
                }
                catch (Exception ex)
                {
                    // Set the connection to null if it was created.
                    if (connection != null)
                    {
                        connection = null;
                    }
                    Console.WriteLine(ex.Message);
                }
            }
            // Return the connection.
            return connection;
        }
        public List<object> DbCommandSelect(string Query)
        {
            string queryString = Query;
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            List<object> lista = new List<object>();
            Connection = CreateDbConnection(Provider, ConnectionString);
            // Check for valid Dbconn.
            if (Connection != null)
            {
                using (Connection)
                {
                    try
                    {
                        // Create the command.
                        DbCommand command = Connection.CreateCommand();
                        command.CommandText = queryString;
                        command.CommandType = CommandType.Text;

                        // Open the Conn.
                        Connection.Open();

                        // Retrieve the data.
                        Reader = command.ExecuteReader();
                        var schema = Reader.GetSchemaTable();

                        schema.Columns.ToString();
                       
                        string[] arr = new string[schema.Rows.Count];
                        while (Reader.Read())
                        {
                            //Console.WriteLine("{0}. {1}", Reader[0], Reader[1]);
                            // for (int i = 0; i < schema.Columns.Count; i++)
                            //{
                            object[] row = new object[schema.Rows.Count];
                            //}
                            Reader.GetValues(row);
                            lista.Add(row);
                            //dictionary.Add(Reader[2].ToString(), Reader[3].ToString());
                        }
                        Console.WriteLine("Query completed successfully!");
                    }

                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception.Message: {0}", ex.Message);
                    }
                }
            }
            else
            {
                Console.WriteLine("Failed: DbConnection is null.");
            }

            Connection.Close();
            return lista;

        }
        public string[] GetCollumnVallues(string Query)
        {
            string[] CollumnValues = null;
            List<string> lista = new List<string>();
            string queryString = Query;
            Connection = CreateDbConnection(Provider, ConnectionString);

            // Check for valid Dbconn.
            if (Connection != null)
            {
                using (Connection)
                {
                    try
                    {
                        // Create the command.
                        DbCommand command = Connection.CreateCommand();
                        command.CommandText = queryString;
                        command.CommandType = CommandType.Text;

                        // Open the Conn.
                        Connection.Open();

                        // Retrieve the data.
                        Reader = command.ExecuteReader();
                        var schema = Reader.GetSchemaTable();

                        CollumnValues = new string[schema.Rows.Count];

                        foreach (DataRow myField in schema.Rows)
                        {
                            lista.Add(myField.ItemArray[0].ToString());

                           
                        }
                        
                    }

                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception.Message: {0}", ex.Message);
                    }
                }
            }
            else
            {
                Console.WriteLine("Failed: DbConnection is null.");
            }

            Connection.Close();
            return lista.ToArray<string>();
        }
    }
}