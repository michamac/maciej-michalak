﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMonitoringApp.Models;
using WebMonitoringApp.Controllers;
using ChartJS.Helpers.MVC;


namespace WebMonitoringApp.Controllers
{
    public class HomeController : Controller
    {
        DBConnectionModel Conn = new DBConnectionModel("System.Data.SqlClient");
        public ActionResult Dashboard(string test)
        {
            string q = test;
            return View();
        }
        public ActionResult Monitoring()
        {
            ViewBag.Message = "Servers Monitoring";

            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult GetServerList(string Query)
        {
            List<object> data = new List<object>();
            data = Conn.DbCommandSelect(Query);

            return PartialView(data);
        }
        public ActionResult ViewChart(string Query, string ServerName, string CounterName)
        {
          
            //TODO - stworzyć odpowiedni model i przerobić na listę

            List<object> data = new List<object>();

            ChartModel chartData = new ChartModel();

            Query = string.Format("{0} '{1}','{2}'",Query,ServerName,CounterName);
            string[] x = Conn.GetCollumnVallues(Query);
            Conn.Reader.Close();
            Conn.Connection.Close();
            var p = Conn.ConnectionString;
            data = Conn.DbCommandSelect(Query);
            object[] a;
               //object[]b;
            object[] tmp;
            int numcol = 0;
            int numrow = data.Count;
            int i = 0;

            // PIVOT WYNIKU DATA
            a = data.ToArray();
            foreach (object[] item in a)
            {
                if (item.Length > numcol)
                    numcol = item.Length;
            }
            //b = new object[numcol + 1];
            //b[0] = x;
            //for (int j = 1; j < numcol+1; j++)
            //{
            //    tmp = new object[numrow];
            //    foreach (object[] itema in a)
            //    {
            //        tmp[i++] = itema[j-1];
            //    }
            //    b[j] = tmp;
            //    i = 0;
            //}

            // time axis labes go to dataset.labels
            tmp = new string[numrow];
            foreach (object[] item in a)
            {
                tmp[i++] = item[0].ToString();
            }
            chartData.Labels = (string[])tmp;
            i = 0;
            
            for (int j  = 1; j < numcol; j++)
            {
                tmp = new object[numrow];
                foreach (object[] itema in a)
                {
                    tmp[i++] = itema[j].ToString();
                }
             
                chartData.Datasets.Add( new ChartModel.Dataset
                {
                    data = tmp,
                    label = x[j],
                    backgroundColor = j.ToString(),
                    borderColor = j.ToString(),
                    borderWidth = 1
                });
                i = 0;
            }
            chartData.ServerName = ServerName;
            chartData.CounterName = CounterName;
            ViewBag.ServerName = ServerName;  

            return PartialView(chartData);


            
        }
   
       public AlertTresholdModels CheckTresholds()
            {
            AlertTresholdModels alertTresholds = new AlertTresholdModels()
            {
                ServerName = "serverName",
                CounterName = "counterName",
                MaxValue = "maxValue",
                MinValue = "minValue",
                Active = true
            };
            //string[] dupa =  new string[1];
            //ViewBag.dupa = new string[] { "aaaaa", "bbbbb", "cccc" };
            return alertTresholds;
            }

        public ChartModel Test(string a, string b)
        {
            ChartModel aaa = new ChartModel();
            aaa.ServerName = a;
            aaa.CounterName = b;
            return ViewBag.aaa = aaa ;
        }
    }
}