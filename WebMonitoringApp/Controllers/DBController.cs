﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMonitoringApp.Models;
using System.Data;
using System.Data.Common;
using System.Configuration;

namespace WebMonitoringApp.Controllers
{
    public class DBController : Controller
    {
        public DBConnectionModel Conn;

        public DataTable GetProviderFactoryClasses()
        {
            // Retrieve the installed providers and factories.
            DataTable table = DbProviderFactories.GetFactoryClasses();

            // Display each row and column value.
            foreach (DataRow row in table.Rows)
            {
                foreach (DataColumn column in table.Columns)
                {
                    Console.WriteLine(row[column]);
                }
            }
            return table;
        }

        // Retrieve a connection string by specifying the providerName.
        // Assumes one connection string per provider in the config file.
        private string GetConnectionStringByProvider(string providerName)
        {
            // Return null on failure.
            string returnValue = null;

            // Get the collection of connection strings.
            ConnectionStringSettingsCollection settings =
                ConfigurationManager.ConnectionStrings;

            // Walk through the collection and return the first 
            // connection string matching the providerName.
            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    if (cs.ProviderName == providerName)
                        returnValue = cs.ConnectionString;
                    break;
                }
            }
            return returnValue;
        }


        // Given a provider name and connection string, 
        // create the DbProviderFactory and DbConnection.
        // Returns a DbConnection on success; null on failure.
        public DbConnection CreateDbConnection(
            string providerName, string connectionString)
        {
            // Assume failure.
            DbConnection connection = null;

            // Create the DbProviderFactory and DbConnection.
            if (connectionString != null)
            {
                try
                {
                    DbProviderFactory factory =
                        DbProviderFactories.GetFactory(providerName);

                    connection = factory.CreateConnection();
                    connection.ConnectionString = connectionString;
                }
                catch (Exception ex)
                {
                    // Set the connection to null if it was created.
                    if (connection != null)
                    {
                        connection = null;
                    }
                    Console.WriteLine(ex.Message);
                }
            }
            // Return the connection.
            return connection;
        }
        private void ExecuteQuery(string HostName)
        {
            Conn.Connection.Open();

            // Create and execute the DbCommand.
            DbCommand command = Conn.Connection.CreateCommand();
            command.CommandText = "null";
            //$"insert into dbo.counters (serverName, CategoryName, CounterName, InstanceName, Value, TimeStamp) values ('{HostName}', '{rc(data.Category)}', '{rc(data.Counter)}', '{rc(data.Instance)}', '{rc((data.Value).ToString())}', SYSDATETIME())";

            int rows = command.ExecuteNonQuery();

            // Display number of rows inserted.
            Console.WriteLine("Inserted {0} rows.", rows);
        }

        //RemoveNotAllowedChars
        static string rc(string str)
        {
            str = new string((from c in str
                              where char.IsLetterOrDigit(c)
                              select c
                             ).ToArray());
            return str;
        }

        public void DbCommandSelect()
        {
            string queryString =
                "SELECT * from dbo.test";

            // Check for valid Dbconn.
            if (Conn != null)
            {
                using (Conn.Connection)
                {
                    try
                    {
                        // Create the command.
                        DbCommand command = Conn.Connection.CreateCommand();
                        command.CommandText = queryString;
                        command.CommandType = CommandType.Text;

                        // Open the Conn.
                        Conn.Connection.Open();

                        // Retrieve the data.
                        Conn.Reader = command.ExecuteReader();
                        while (Conn.Reader.Read())
                        {
                            Console.WriteLine("{0}. {1}", Conn.Reader[0], Conn.Reader[1]);
                        }
                        Console.WriteLine("Query completed successfully!");
                    }

                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception.Message: {0}", ex.Message);
                    }
                }
            }
            else
            {
                Console.WriteLine("Failed: DbConnection is null.");
            }
        }

        // GET: DB
        public ActionResult Test()
        {
            Test abc = new Test { row = new object[2] };
            
            return PartialView(abc);
        }
        public ActionResult DBQuery(string DBQuery)
        {

            return new EmptyResult();
        }
    }
}